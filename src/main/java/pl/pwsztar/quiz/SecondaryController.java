package pl.pwsztar.quiz;

import java.io.BufferedReader;
import java.io.IOException;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import pl.pwsztar.quiz.model.AnswerData;
import pl.pwsztar.quiz.model.QuestionData;

import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class SecondaryController {

    public Label number_of_question;
    public Label question;
    public Label points;
    public CheckBox answer_1;
    public CheckBox answer_2;
    public CheckBox answer_3;
    public CheckBox answer_4;
    public Button secondaryButton;
    public Label errorInfo;

    private int questionId;
    private boolean lastQuestion;


    @FXML
    public void nextQuestion() throws IOException {

        SoundEffect.click();

        if (!App.isConnected()) {
            System.err.println("Utracono połączenie!");
            errorInfo.visibleProperty().setValue(true);
            errorInfo.setText("Wystąpił błąd!\n" +
                    "Sprawdź połączenie z Internetem i spróbuj ponownie.");
            return;
        }

        errorInfo.visibleProperty().setValue(false);

        AnswerData answer = readAnswers();
        sendAnswer(answer);
        clearCheckBoxes();

        if(!lastQuestion) {
            questionId++;
            setQuestion(questionId);
        } else
            App.setRoot("third");

    }

    @FXML
    private void initialize() {

        questionId = 1;
        setQuestion(questionId);

    }

    private void clearCheckBoxes() {
        answer_1.selectedProperty().setValue(false);
        answer_2.selectedProperty().setValue(false);
        answer_3.selectedProperty().setValue(false);
        answer_4.selectedProperty().setValue(false);
    }


    private QuestionData fetchQuestion (int questionId) {
        final HttpClient client = HttpClientBuilder.create().build();
        final HttpGet request = new HttpGet("http://127.0.0.1:8080/quiz/question/"+questionId);

        final Gson gson = new Gson();

        try {

            final HttpResponse response = client.execute(request);  // Otrzymujemy odpowiedz od serwera.
            final HttpEntity entity = response.getEntity();

            final String json = EntityUtils.toString(entity);   // Na tym etapie odczytujemy JSON'a, ale jako String.

            // Wyswietlamy zawartosc JSON'a na standardowe wyjscie.
            System.out.println("Odczytano JSON'a:");
            System.out.println(json);

            // zamiana Stringa na obiekt QuestionData
            final Type type = new TypeToken<QuestionData>(){}.getType();
            final QuestionData question = gson.fromJson(json, type);

            question.isLastQuestion = json.contains("true");

            this.lastQuestion = question.isLastQuestion;

            return question;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void setQuestion (int questionId) {
        QuestionData questionData = fetchQuestion(questionId);

        if (questionData != null) {
            String[] answers = questionData.getAnswers().toArray(new String[0]);

            number_of_question.setText("Pytanie nr: " + questionId);
            question.setText(questionData.getQuestion());
            points.setText("Punkty do zdobycia za pytanie: " + questionData.getPoints());
            answer_1.setText(answers[0]);
            answer_2.setText(answers[1]);
            answer_3.setText(answers[2]);
            answer_4.setText(answers[3]);

            System.out.println("Wyświetlono pytanie z id = " + questionId);
        } else {
            System.out.println("Wystąpił błąd z pobieraniem pytania");
        }
    }

    private AnswerData readAnswers() {
        int id = questionId;
        List<Integer> answers = new ArrayList<>();

        if (answer_1.isSelected()) {
            answers.add(1);
        } if (answer_2.isSelected()) {
            answers.add(2);
        } if (answer_3.isSelected()) {
            answers.add(3);
        } if (answer_4.isSelected()) {
            answers.add(4);
        }

        return new AnswerData(id, answers);
    }

    private void sendAnswer(AnswerData answer) throws IOException {
        System.out.println("Wysyłam odpowiedź...");
        URL url = new URL("http://127.0.0.1:8080/quiz/calculate");
        HttpURLConnection con = (HttpURLConnection)url.openConnection();
        con.setRequestMethod("PUT");
        con.setRequestProperty("Content-Type", "application/json; utf-8");
        con.setRequestProperty("Accept", "application/json");
        con.setDoOutput(true);

        String json = "{" +
                        "\"questionId\": " + answer.getId() + ", " +
                        "\"selectedAnswers\": " + answer.getAnswers() +
                        "}";

        try(OutputStream os = con.getOutputStream()) {
            byte[] input = json.getBytes(StandardCharsets.UTF_8);
            os.write(input, 0, input.length);
        }


        try(BufferedReader br = new BufferedReader(
                new InputStreamReader(con.getInputStream(), StandardCharsets.UTF_8))) {
            StringBuilder response = new StringBuilder();
            String responseLine;
            while ((responseLine = br.readLine()) != null) {
                response.append(responseLine.trim());
            }
            System.out.println(response);
        }

    }

}
