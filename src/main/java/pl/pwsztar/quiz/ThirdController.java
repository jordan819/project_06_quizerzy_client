package pl.pwsztar.quiz;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;


import java.io.File;
import java.io.IOException;
import java.awt.Desktop;

public class ThirdController {

    public Label result;

    @FXML
    private void switchToFirst() throws IOException {
        SoundEffect.click();
        App.setRoot("primary");
    }

    @FXML
    private void initialize() throws IOException {

        boolean isPassed = getReport();
        if (isPassed) {
            result.setText("Quiz zdany!");
            result.setStyle("-fx-text-fill: green;");
            SoundEffect.success();
        } else {
            result.setText("Quiz niezdany...");
            result.setStyle("-fx-text-fill: red;");
            SoundEffect.failure();
        }

        openReport();

    }

    private boolean getReport() throws IOException {
        System.out.println("Pobieram raport...");
        final HttpClient client = HttpClientBuilder.create().build();
        final HttpPost request = new HttpPost("http://127.0.0.1:8080/quiz/report/");
        final HttpResponse response = client.execute(request);

        return response.getStatusLine().getStatusCode() == 200;
    }

    private void openReport() {
        File report = getLastModified();
        if (Desktop.isDesktopSupported()) {
            try {
                Desktop.getDesktop().open(report);
            } catch (IOException ex) {
                System.err.println("Brak domyślnego programu do otworzenia pliku .pdf");
            }
        }
    }

    public static File getLastModified() {
        final String DIRECTORY = "C:\\temp\\";
        File directory = new File(DIRECTORY);
        File[] files = directory.listFiles(File::isFile);
        long lastModifiedTime = Long.MIN_VALUE;
        File chosenFile = null;

        if (files != null)
        {
            for (File file : files)
            {
                if (file.lastModified() > lastModifiedTime)
                {
                    chosenFile = file;
                    lastModifiedTime = file.lastModified();
                }
            }
        }

        return chosenFile;
    }

}
