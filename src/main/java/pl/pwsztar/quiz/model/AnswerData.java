package pl.pwsztar.quiz.model;

import java.util.List;

public class AnswerData {

    private final int id;
    private final List<Integer> answers;

    public AnswerData(int id, List<Integer> answers) {
        this.id = id;
        this.answers = answers;
    }

    public int getId() {
        return id;
    }

    public List<Integer> getAnswers() {
        return answers;
    }
}
