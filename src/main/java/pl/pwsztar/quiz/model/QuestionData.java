package pl.pwsztar.quiz.model;

import java.io.Serializable;
import java.util.List;

public class QuestionData implements Serializable {
    public String question;
    public List<String> answers;
    public int points;
    public boolean isLastQuestion;

    public QuestionData() {

    }

    public QuestionData(String question, List<String> answers, int points, boolean isLastQuestion) {
        this.question = question;
        this.answers = answers;
        this.points = points;
        this.isLastQuestion = isLastQuestion;
    }

    public String getQuestion() {
        return question;
    }

    public List<String> getAnswers() {
        return answers;
    }

    public int getPoints() {
        return points;
    }

    public boolean isLastQuestion() {
        return isLastQuestion;
    }

}
