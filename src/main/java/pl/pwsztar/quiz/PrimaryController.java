package pl.pwsztar.quiz;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.scene.control.Label;

public class PrimaryController {

    public Label errorInfo;

    @FXML
    private void switchToSecondary() throws IOException {
        SoundEffect.click();

        if (App.isConnected()) {
            errorInfo.visibleProperty().setValue(false);
            App.setRoot("secondary");
        } else {
            errorInfo.visibleProperty().setValue(true);
            errorInfo.setText("Wystąpił błąd!\n" +
                    "Sprawdź połączenie z Internetem i spróbuj ponownie.");
        }
    }

}
