package pl.pwsztar.quiz;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.IOException;

/**
 * W celu uruchomienia programu, w Project Structure należy dodać wszystkie biblioteki z folderu lib,
 * a następnie w zakładce Maven wybrać Reload All Maven Projects.
 */
public class App extends Application {

    private static Scene scene;

    @Override
    public void start(Stage stage) throws IOException {
        scene = new Scene(loadFXML("primary"), 600, 400);
        stage.setScene(scene);
        stage.show();
        stage.setResizable(false);
    }

    static void setRoot(String fxml) throws IOException {
        scene.setRoot(loadFXML(fxml));
    }

    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(fxml + ".fxml"));
        return fxmlLoader.load();
    }

    public static void main(String[] args) {
        launch();
    }

    public static boolean isConnected() throws IOException {
        try {
            final HttpClient client = HttpClientBuilder.create().build();
            final HttpGet request = new HttpGet("http://127.0.0.1:8080/quiz/test");
            client.execute(request);
            System.out.println("Serwer działa");
        } catch (HttpHostConnectException e) {
            System.out.print("Serwer nie działa lub brak połączenia: ");
            System.out.println(e.getMessage());
            return false;
        }
        return true;
    }

}