package pl.pwsztar.quiz;


import javax.sound.sampled.*;
import java.io.File;

public class SoundEffect {

    private static String filePath;

    public static void failure() {
        filePath = "src\\main\\resources\\audio\\slow-sad-trombone.wav";
        play(filePath);
    }

    public static void success() {
        filePath = "src\\main\\resources\\audio\\applause.wav";
        play(filePath);
    }

    public static void click() {
        filePath = "src\\main\\resources\\audio\\pop-click.wav";
        play(filePath);
    }

    private static void play(String filePath) {

        try {
            AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File(filePath).getAbsoluteFile());

            Clip clip = AudioSystem.getClip();
            clip.open(audioInputStream);
            clip.start();
        } catch (Exception e) {
            System.err.println("Problem z odtworzeniem dźwięku: " + e.getMessage());
        }
    }
}
