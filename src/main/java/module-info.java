module pl.pwsztar.quiz {
    requires javafx.controls;
    requires javafx.fxml;
    requires gson;
    requires httpcore;
    requires httpclient;
    requires java.sql;
    requires java.desktop;

    opens pl.pwsztar.quiz to javafx.fxml;
    exports pl.pwsztar.quiz;
    exports pl.pwsztar.quiz.model;
}